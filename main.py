# All packaages that will be required
import cv2
from sklearn.cluster import KMeans
import numpy as np
from os import listdir,path
import argparse
from database import color_db

#important functions
def read_data(directory):
    data = []
    files_list = listdir(directory)
    for i , file_name in enumerate(files_list):
        temp_img = cv2.imread(path.join(directory ,file_name))
        temp_img = cv2.resize(temp_img, (min(800,temp_img.shape[1]) , min(600,temp_img.shape[0])))
        temp_img = cv2.cvtColor(temp_img , cv2.COLOR_BGR2RGB)
        data.append({"img_name": file_name , "img_data": temp_img , "height": temp_img.shape[0], "width": temp_img.shape[1]})
        data[i]["img_data"] = data[i]["img_data"].reshape((data[i]["img_data"].shape[0] * data[i]["img_data"].shape[1], 3))
    return data


#K-means on a list that stores all the data points
def Kmeans(data , num_clusters):
    clt = KMeans(n_clusters = num_clusters)
    clt.fit(data)
    return clt



#Extended Connectivity algorithm class
class ExtendedConnectivity:

    def __init__(self , label, sz, clusters):
        self.visit = np.zeros(label.shape , dtype = int)
        self.comp = np.zeros(label.shape , dtype = int)
        self.counter = 0
        self.r,self.c = label.shape
        self.label = label
        self.sz = sz
        self.clusters = clusters
        
    def check(self , x , y, val):
        if x < 0 or y < 0 or x >= self.r or y >= self.c or self.label[x][y] != val or self.visit[x][y] == 1:
            return 0
        else:
            return 1
    
    def dfs(self , x , y, val):
        st = []
        st.append([x , y])
        while len(st) > 0:
            x,y = st.pop()
            self.visit[x][y] = 1
            self.comp[x][y] = self.counter
            for i in range(int(-self.sz / 2) , int((self.sz + 1)/2)):
                for j in range(int(-self.sz / 2) , int((self.sz + 1)/2)):
                    if self.check(x + i , y + j , val) == 1:
                        self.visit[x + i][y + j] = 1
                        st.append([x + i , y + j])

    def extended_connectivity(self):
        for i in range(0 , self.r):
            for j in range(0 , self.c):
                if self.visit[i][j] == 0:
                    self.counter += 1
                    self.dfs(i , j , self.label[i][j])
    
    def component_details(self):
        self.comp_info = [{"size":0 , "min_h": 1000000000 , "max_h" : -1 , 
                           "min_w" : 1000000000 , "max_w": -1 , "color":-1} for i in range(0 , self.counter + 2)]
        for i in range(0 , self.r):
            for j in range(0 , self.c):
                idx = int(self.comp[i][j])
                self.comp_info[idx]["size"] += 1
                self.comp_info[idx]["min_h"] = min(self.comp_info[idx]["min_h"] , i)
                self.comp_info[idx]["max_h"] = max(self.comp_info[idx]["max_h"] , i)
                self.comp_info[idx]["min_w"] = min(self.comp_info[idx]["min_w"] , j)
                self.comp_info[idx]["max_w"] = max(self.comp_info[idx]["max_w"] , j)
                self.comp_info[idx]["color"] = list(self.clusters[self.label[i][j]])
    
    def get_visible_colors(self , size_threshold , area_ratio):
        self.component_details()
        visible_colors = []
        for comp_info in self.comp_info:
            if comp_info["size"] >= size_threshold:
                total_area = (comp_info["max_h"] - comp_info["min_h"]) * (comp_info["max_w"] - comp_info["min_w"]) 
                occupied_area = comp_info["size"]
                if total_area == 0:
                    continue
                percentage = float(occupied_area) / total_area;
                if percentage >= area_ratio:
                    visible_colors.append(comp_info["color"])
        return visible_colors
                    




def color_detection_model_train():
    visible_colors = []
    input_data = read_data('Dataset/Train/grey/')
    for image_data in input_data:
        print "Currently training on file: " + image_data["img_name"],
        KMEANS = Kmeans(image_data["img_data"] , 15)
        #print (KMEANS.cluster_centers_)
        KMEANS.labels_ = KMEANS.labels_.reshape((image_data["height"] , image_data["width"]))
        ec = ExtendedConnectivity(KMEANS.labels_ , 5 , KMEANS.cluster_centers_)
        ec.extended_connectivity()
        temp_color_set = ec.get_visible_colors(50 , 0.1)
        for color in temp_color_set:
            if color not in visible_colors:
                visible_colors.append(color)
        print "...Done"
        print "Colors detected : " + str(len(temp_color_set)) +" Total Colors: " + str(len(visible_colors))
        #print visible_colors
    return visible_colors

def calculate_L2(color_a, color_b):
    return float(abs(color_a[0] - color_b[0])**2 + abs(color_a[1] - color_b[1])**2 + abs(color_a[2] - color_b[2])**2)**(0.5)
        
def color_detection_model_test(color_set, input_path, threshold):
    img = cv2.imread(input_path)
    img = cv2.resize(img, (min(800,img.shape[1]) , min(600,img.shape[0])))
    img = cv2.cvtColor(img , cv2.COLOR_BGR2RGB)
    reshaped_img = img.reshape((img.shape[0] * img.shape[1], 3))
    KMEANS = Kmeans(reshaped_img , 15)
    KMEANS.labels_ = KMEANS.labels_.reshape((img.shape[0] , img.shape[1]))
    ec = ExtendedConnectivity(KMEANS.labels_ , 5 , KMEANS.cluster_centers_)
    ec.extended_connectivity()
    temp_color_set = ec.get_visible_colors(50 , 0.1)
    flag= False
    max_diff = -1
    for color in temp_color_set:
        min_diff = 99999999999999999
        for color_db in color_set:
                min_diff = min(min_diff, calculate_L2(color,color_db))
                #print min_diff
        if min_diff != 99999999999999999:
            max_diff = max(max_diff, min_diff)
        if min_diff > threshold:
            flag =  True # color
    return flag,max_diff

'''
def read_database():
    import re
    f = open('database.txt','r')
    line = f.read()
    f.close()
    line = line[1:-1]
    x = [re.search('(?!.*\[).*',entry).group() for entry in line.split('],')]
    l = list()
    for s in x:
        if s.strip()[-1] == ']':
            s = s[:-1]
            z = [float(i.strip()) for i in s.split(',')]
            l.append(z)
    return l
'''
def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required = True, help = "Path to the image")
    args = vars(ap.parse_args())

    if path.isfile('database.txt'):
        color_set = color_db
    else:
        print "Training"
        color_set = color_detection_model_train()
        f = open('database.txt','w')
        f.write(str(color_set))
        f.close()
    threshold = 10
    colored, difference = color_detection_model_test(color_set, args['image'], float(10))
    if colored == True:
        print "image Colored", difference
    else:
        print "image grey",difference

if __name__ == "__main__":
    main()
